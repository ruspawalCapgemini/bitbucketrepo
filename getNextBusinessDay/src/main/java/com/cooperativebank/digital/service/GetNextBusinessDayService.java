package com.cooperativebank.digital.service;

import java.text.ParseException;


public interface GetNextBusinessDayService {
	
	String getNextBusinessDate(String dateStr) throws ParseException;
	
	String getCurrentDate();
	
	boolean isDateValid(String date);
	
	String getDay(String date);



}
