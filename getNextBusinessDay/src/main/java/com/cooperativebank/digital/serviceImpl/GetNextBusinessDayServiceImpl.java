package com.cooperativebank.digital.serviceImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.cooperativebank.digital.service.GetNextBusinessDayService;

@Component
public class GetNextBusinessDayServiceImpl implements GetNextBusinessDayService {

	String DATE_FORMAT = "yyyy-MM-dd";

	@Override
	public String getCurrentDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		Date date = new Date();
		return dateFormat.format(date);
	}

	@Override
	public String getNextBusinessDate(String dateStr)
			throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		Date date = null;
		try {
			date = dateFormat.parse(dateStr);
		} catch (ParseException e) {
			throw e;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_WEEK, 1);
		while (!isWorkingDay(calendar.getTime(), calendar)) {
			calendar.add(Calendar.DAY_OF_WEEK, 1);
		}
		return dateFormat.format(calendar.getTime());
	}

	private boolean isWorkingDay(Date date, Calendar calendar) {
		calendar.setTime(date);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		if ((dayOfWeek == 7) || (dayOfWeek == 1)) {
			return false;
		}
		return true;
	}

	@Override
	public boolean isDateValid(String date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(date);
			return true;
		} catch (ParseException e) {
			return false;
		}

	}

	@Override
	public String getDay(String date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		Date parsedDate = null;
		try {
			parsedDate = dateFormat.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat format2 = new SimpleDateFormat("EEEE");
		return format2.format(parsedDate);
	}

}
