package com.cooperativebank.digital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GetNextBusinessDayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GetNextBusinessDayApplication.class, args);
	}

}
