package com.cooperativebank.digital.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NextBusinessDayResponse {
	
	@JsonProperty
	String date;
	@JsonProperty
	String day;
	@JsonProperty
	String description;
	
	public NextBusinessDayResponse(String date, String day, String description) {
		super();
		this.date = date;
		this.day = day;
		this.description = description;
	}

	@Override
	public String toString() {
		return "NextBusinessDayResponse [date=" + date + ", day=" + day
				+ ", description=" + description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NextBusinessDayResponse other = (NextBusinessDayResponse) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (day == null) {
			if (other.day != null)
				return false;
		} else if (!day.equals(other.day))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}
	

}
