package com.cooperativebank.digital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cooperativebank.digital.dto.ErrorResponse;
import com.cooperativebank.digital.dto.NextBusinessDayResponse;
import com.cooperativebank.digital.service.GetNextBusinessDayService;

/**
 * Microservice to get next business day
 * 
 * @author ruspawal
 *
 */
@RestController
public class GetNextBusinessDayController {

	@Autowired
	GetNextBusinessDayService getNextBusinessDayService;

	@RequestMapping(value = "/next-working-day", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object getNextBusinessDay(
			@RequestParam(name = "after", required = false) String date) {
		String next_Date = null;
		NextBusinessDayResponse response = null;
		ErrorResponse errorResponse = null;

		try {
			if (date == null) {
				// Use current date when date is not provided in request
				next_Date = getNextBusinessDayService
						.getNextBusinessDate(getNextBusinessDayService
								.getCurrentDate());
			} else {
				// Check if given date is valid or not
				if (!getNextBusinessDayService.isDateValid(date)) {
					errorResponse = new ErrorResponse(
							"The after date is not a valid date",
							HttpStatus.BAD_REQUEST);
					return new ResponseEntity(errorResponse,
							HttpStatus.BAD_REQUEST);
				}
				next_Date = getNextBusinessDayService.getNextBusinessDate(date);
			}

			response = new NextBusinessDayResponse(next_Date,
					getNextBusinessDayService.getDay(next_Date),
					"The next working day");
			return new ResponseEntity(response, HttpStatus.OK);

		} catch (Exception e) {
			errorResponse = new ErrorResponse(
					"Please enter date in correct format",
					HttpStatus.UNPROCESSABLE_ENTITY);
			return new ResponseEntity(errorResponse,
					HttpStatus.UNPROCESSABLE_ENTITY);
		}

	}

}
