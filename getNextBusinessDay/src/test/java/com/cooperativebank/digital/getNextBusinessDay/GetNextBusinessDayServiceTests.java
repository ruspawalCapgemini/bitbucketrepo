package com.cooperativebank.digital.getNextBusinessDay;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.cooperativebank.digital.serviceImpl.GetNextBusinessDayServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class GetNextBusinessDayServiceTests {
	
	@InjectMocks
	private GetNextBusinessDayServiceImpl serviceImpl;
	
	/*
	 * Need to pass current date to expectedDate to execute test case successfully
	 */
	@Test
	public void testGetCurrentDate(){
		String expectedDate="2020-01-21";
		String actualDate= serviceImpl.getCurrentDate();
		
		assertEquals(expectedDate, actualDate);
	}
	
	@Test
	public void testGetNextBusinessDate(){
		String expectedBusinessDate="2020-01-21";
		String actualBusinessDate=null;
		try {
			actualBusinessDate = serviceImpl.getNextBusinessDate("2020-01-20");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		assertEquals(expectedBusinessDate, actualBusinessDate);
	}
	
	@Test
	public void testGetNextBusinessDateOnNonBusinessDateProvided(){
		String expectedBusinessDate="2020-01-27";
		String actualBusinessDate=null;
		try {
			actualBusinessDate = serviceImpl.getNextBusinessDate("2020-01-24");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		assertEquals(expectedBusinessDate, actualBusinessDate);
	}
	
	@Test(expected = ParseException.class)
	public void testGetNextBusinessDateParseExceptionOccurred() throws ParseException{
		String actualBusinessDate = serviceImpl.getNextBusinessDate("2020-01--");
	}
	
	
	@Test
	public void testIsDateValidSuccess(){
		boolean expectedValidDate=true;
		boolean actualValidDate=serviceImpl.isDateValid("2020-01-20");
		
		assertEquals(expectedValidDate, actualValidDate);
	}
	
	@Test
	public void testIsDateValidFalseCheck(){
		boolean expectedValidDate=false;
		boolean actualValidDate=serviceImpl.isDateValid("2020-01-35");
		
		assertEquals(expectedValidDate, actualValidDate);
	}
	
	@Test
	public void testGetDay(){
		String expectedDay="Tuesday";
		String actualDay=serviceImpl.getDay("2020-01-21");
		
		assertEquals(expectedDay, actualDay);
	}

}
