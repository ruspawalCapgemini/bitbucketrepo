package com.cooperativebank.digital.getNextBusinessDay;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.text.ParseException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.cooperativebank.digital.controller.GetNextBusinessDayController;
import com.cooperativebank.digital.dto.ErrorResponse;
import com.cooperativebank.digital.dto.NextBusinessDayResponse;
import com.cooperativebank.digital.service.GetNextBusinessDayService;

@RunWith(MockitoJUnitRunner.class)
public class GetNextBusinessDayApplicationTests {

	@Mock
	private GetNextBusinessDayService getNextBusinessDayService;
	
	@InjectMocks
	private GetNextBusinessDayController controller;
	
	@Test
	public void testGetNextBusinessDaySuccess() {
		
		NextBusinessDayResponse nextBusinessDayResponse = new NextBusinessDayResponse("2020-01-21", "Tuesday", "The next working day");
		ResponseEntity expectedresponseEntity= new ResponseEntity(nextBusinessDayResponse, HttpStatus.OK);
		
		try {
			when(getNextBusinessDayService.getNextBusinessDate(new String("2020-01-20"))).thenReturn("2020-01-21");
			when(getNextBusinessDayService.getDay("2020-01-21")).thenReturn("Tuesday");
			when(getNextBusinessDayService.isDateValid(new String("2020-01-20"))).thenReturn(true);
			
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		ResponseEntity actualResponseEntity=(ResponseEntity) controller.getNextBusinessDay("2020-01-20");
			
		assertEquals(expectedresponseEntity, actualResponseEntity);
	}
	
	@Test
	public void testGetNextBusinessDayFailure() {
		
		ErrorResponse errorResponse = new ErrorResponse("The after date is not a valid date", HttpStatus.BAD_REQUEST);
		ResponseEntity expectedresponseEntity= new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
		ResponseEntity actualResponseEntity=(ResponseEntity) controller.getNextBusinessDay("2020-01-35");
		
		assertEquals(expectedresponseEntity, actualResponseEntity);
	}
	
	@Test
	public void testGetNextBusinessDaySuccessIfDateNotProvided() {
		
		NextBusinessDayResponse nextBusinessDayResponse = new NextBusinessDayResponse("2020-01-22", "Wednesday", "The next working day");
		ResponseEntity expectedresponseEntity= new ResponseEntity(nextBusinessDayResponse, HttpStatus.OK);
		
		try {
			when(getNextBusinessDayService.getCurrentDate()).thenReturn("2020-01-21");
			when(getNextBusinessDayService.getNextBusinessDate(new String("2020-01-21"))).thenReturn("2020-01-22");
			when(getNextBusinessDayService.getDay("2020-01-22")).thenReturn("Wednesday");
			
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		ResponseEntity actualResponseEntity=(ResponseEntity) controller.getNextBusinessDay(null);
			
		assertEquals(expectedresponseEntity, actualResponseEntity);
	}

}
