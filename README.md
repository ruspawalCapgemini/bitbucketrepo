**Get Next Business Day application

This Spring Boot application returns the next business day by taking the given date as input.
If no date is provided then, it will take current date as input and return the next business date to it.

**Steps to build the application**
Open command prompt and go to folder where application pom.xml file exists
Run the below command to build the application
mvn clean install

**Steps to run the application**
Import the application in Eclipse as an Existing Maven Projects.
Go to the GetNextBusinessDayApplication.java file and run the java application as
Run As -> Java Application

The application will run on embedded Tomcat server on port 8080.

**Sample REST Request and Response**
*1. After date is provided*
*Request:*
http://localhost:8080/next-working-day?after=2020-01-31

*Response:*
{
   "date": "2020-02-03",
   "day": "Monday",
   "description": "The next working day"
}

*2. When after date is not provided(It will take current date)*

*Request:*
http://localhost:8080/next-working-day

*Response:*
{
   "date": "2020-01-20",
   "day": "Monday",
   "description": "The next working day"
}

**Error Scenarios**
*1.Invalid date is given* 

Request:
http://localhost:8080/next-working-day?after=2020-01-35

Response:
{
   "message": "The after date is not a valid date",
   "status": "BAD_REQUEST"
}





